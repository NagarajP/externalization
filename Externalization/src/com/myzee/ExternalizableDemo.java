package com.myzee;

import java.io.Externalizable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class ExternalizableDemo {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("serialization");
		Employee e =  new Employee(12, "nagaraj", "npattar@gmail.com", 25);
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("ext.ser"));
		oos.writeObject(e);
		System.out.println(e);
		
		System.out.println("\ndeserialization");
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("ext.ser"));
		Employee e1 = (Employee)ois.readObject();
		System.out.println(e1);
	}

}

class Employee implements Externalizable{
	int id;
	String name;
	String email;
	int age;
	
	public Employee() {
		System.out.println("default constructor invoked");
	}
	
	public Employee(int id, String name, String email, int age) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.age = age;
		System.out.println("argumented constructor invoked");
	}
	
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("readExternal() invoked");
		id = in.readInt();
		name = (String)in.readObject();
//		email = (String)in.readObject();	//returns null, coz email has not be serialized in writeExtern() method.
		age = in.readInt();
	}
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("writeExternal() invoked");
		out.writeInt(id);
		out.writeObject(name);
//		out.writeObject(email);
		out.writeInt(age);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id + ", " + this.name + ", " + this.email + ", " + this.age;
	}
}
